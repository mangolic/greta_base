#############################################
# CONFIG
cmd="curl"
protocol="http"
#host="127.0.0.1" #from node
host=$(boot2docker ip 2> /dev/null) #from host to local docker
#host="80.240.138.39" #from host to digitalocean
port=5005
resource="data"
#############################################

#TEST
#resource="test"
$cmd $protocol://$host:$port/$resource
$cmd $protocol://$host:$port/$resource/123
exit 1;

echo "***\nINSERT"
values="key=user&value=test"
key=`$cmd $protocol://$host:$port/$resource -d $values -X POST ` # -v # verbose
echo "received key '$key'"

exit 1




## AUTH
# test access to /login
echo "***\nAUTH"
auth="user=admin&password=passworda"
#values="$auth&key=user&value=test"
key=`$cmd $protocol://$host:$port/login -d $auth -X POST ` # -v # verbose
echo "received response '$key'"

exit




#############################################
#ADD a new element
echo "***\nINSERT"
values="key=user&value=test"
key=`$cmd $protocol://$host:$port/$resource -d $values -X POST ` # -v # verbose
echo "received key '$key'"

#ADD a new element forcing the key
# WARNING: this works as an update if already exists
values="key=food&value=c4ppucc1n0"
# generate a random key

# N.B. shuf is available in Ubuntu, not OsX
# INSTALLATO gshuf da coreutils (brew) e link a  "shuf" sul MAC
fix=`shuf -i 1000-50000 -n 1`

echo "***\nINSERT with key '$fix'"
#exec
id=`$cmd $protocol://$host:$port/$resource -d "$values&id=$fix" -X POST `
echo "received key '$id' ;) "

#GET the full list
echo "***\nFULL LIST"
$cmd $protocol://$host:$port/$resource

#GET the single element
echo "***\nGET the single element [ key '$key' ]"
$cmd $protocol://$host:$port/$resource/$key
echo "***\nGET the single element [ key '$id' ]"
$cmd $protocol://$host:$port/$resource/$id

#DELETE an element
echo "***\nDELETE a single element [ key '$key' ]"
$cmd $protocol://$host:$port/$resource/$key -X DELETE
#echo "***\nDELETE non existing single element [ key 'a$key' ]"
#$cmd $protocol://$host:$port/$resource/a$key -X DELETE

echo "***\n\nDONE"
exit 0;

#UPDATE
curl http://localhost:5000/todos/todo3 -d "task=something different" -X PUT -v
