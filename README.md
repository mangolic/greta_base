# README #

G.R.E.T.A.
Genomic Resource Environment with Toolbox and API

 -- My GIT init repo for a GREAT project --

### What is this repository for? ###

* Quick summary

Prototyping a new big idea on Big Data

* Version

start with 0.1

### What will the project have? ###

* A web UI (foundation6/app)
* A front-end JS framework (Angularjs?)
* A REST API back-end(PHP, slim or fatfree)
* Resources:
    * Maria/Mysql DB (ORM?)
    * Mongo DB (ODM Morph?)
    * FS (python lib?)
    * Meta-scripting for cluster codes (python Plumbum)
    * Jobs queuing (RabbitMQ?)

***

== modify me in the future: ==

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact