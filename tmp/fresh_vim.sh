########################################################
## LATEST VIM 7.4 - medium.com/paolo.donorio - 09_2014

#COOL PLUGINS:
#https://github.com/terryma/vim-multiple-cursors
#SYNC CONF across machines
#http://www.harecoded.com/synchronize-your-vim-configuration-across-different-machines-1295014

#choose path?
myinstallp="./mynewvim_package"
rm -rf $myinstallp
mkdir -p $myinstallp
cd $myinstallp
p=`pwd`
echo "Using dir $p - Downloading"

#backup your configuration
mkdir backup
cp -r $HOME/.vim backup/_vim.dir
cp $HOME/.vimrc backup/_vimrc

echo "DEBUG EXIT"; exit;

## DOWNLOAD
#download vim bin UNIX
wget ftp://ftp.vim.org/pub/vim/unix/vim-7.4.tar.bz2
#dropbox my conf
wget "http://bit.ly/vim74conf" -O latestvimconf.zip     #paulie conf
#vundle plugin
git clone https://github.com/gmarik/Vundle.vim.git Vundle.vim   #.vim/bundle/

#decompress
echo "Decompress"
tar xvzf vim*tar.bz2
unzip latestvimconf.zip -d ./vimconf
#remove builds
rm *.zip *.bz?
mv vim7? newvim

#compile
echo "Ready to compile"
sleep 3
cd newvim
make unixall
make

#set VIM alias?
alias vim='$p/newvim/src/vim'
#use with no plugin?
vim --noplugin

#install vundle
#mv Vundle.vim

#install plugins
#vim +BundleInstall +qall 2>/dev/null
exit;

#2 aliases: normal vim (already with sessions)
#no session vim
