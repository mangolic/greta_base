#!/bin/bash
set -e

chown -R mysql:mysql /var/lib/mysql

if [ -z "$(ls -A /var/lib/mysql)" -a "${1%_safe}" = 'mysqld' ]; then
    if [ -z "$MYSQL_ROOT_PASSWORD" ]; then
        echo >&2 'error: database is uninitialized and MYSQL_ROOT_PASSWORD not set'
        echo >&2 '  Did you forget to add -e MYSQL_ROOT_PASSWORD=... ?'
        exit 1
    fi

    mysql_install_db --user=mysql --datadir=/var/lib/mysql

    # These statements _must_ be on individual lines, and _must_ end with
    # semicolons (no line breaks or comments are permitted).
    # TODO proper SQL escaping on ALL the things D:
    TEMPFILE='/tmp/mysql-first-time.sql'
    echo "" > $TEMPFILE
    #cat > "$TEMPFILE" << EOSQL
    echo "DELETE FROM mysql.user;" >> $TEMPFILE
    echo "CREATE USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}' ;" >> $TEMPFILE
    echo "GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION;" >> $TEMPFILE
    echo "DROP DATABASE IF EXISTS test;" >> $TEMPFILE

    if [ "$MYSQL_DATABASE" ]; then
        echo "CREATE DATABASE IF NOT EXISTS $MYSQL_DATABASE ;" >> "$TEMPFILE"
    fi

    if [ "$MYSQL_USER" -a "$MYSQL_PASSWORD" ]; then
        echo "CREATE USER '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD' ;" >> "$TEMPFILE"

        if [ "$MYSQL_DATABASE" ]; then
            echo "GRANT ALL ON $MYSQL_DATABASE.* TO '$MYSQL_USER'@'%' ;" >> "$TEMPFILE"
        fi
    fi

    echo 'FLUSH PRIVILEGES ;' >> "$TEMPFILE"

    set -- "$@" --init-file="$TEMPFILE"
fi

chown -R mysql:mysql /var/lib/mysql
exec "$@"