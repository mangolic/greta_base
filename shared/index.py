# === Project directions ===

"""
***Docker containers are great.***
I wanted to create a basic project with docker containers
for developing a web app in python for good.
"""

# === Project technologies ===

"""
* Docker virtual environment
* Python and Flask framework for API dev
* RethinkDB and RethinkDB python ORM as data storage
* Angularjs for Frontend web app
* Bootstrap for CSS
"""

# === What i excpect you would be able to do: ===

"""
1. Launch with ease Docker containers
2. Build and launch a db container, a python container, a web container already linked privately
3. Share the needed dirs with container to allow live editing
4. Write great python code with snippets to ease documentation
5. Have fun :)
"""

# === Installation ===

"""
...to do!

For a quick start on the use of Pycco for auto-doc html and markdown on python
https://realpython.com/blog/python/generating-code-documentation-with-pycco/
p.s. don't forget to check the watchdog feature in the end ;)
"""

# === Python Rethinkdb code tutorial ===

"""

You should start with the main file [[rdb_handler.py]]

"""