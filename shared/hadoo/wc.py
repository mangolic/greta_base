# === Just a mrjob test ===

"""
Testing hadoop and python on a docker container

**How to launch?**
You can pass multiple input files, mixed with stdin (using the - character):

`$ python my_job.py input1.txt input2.txt - < input3.txt`
"""

from mrjob.job import MRJob

class MRWordCounter(MRJob):
    """
    A job is defined by a class that inherits from MRJob.
    This class contains methods that define the steps of your job.

    A step consists of a **mapper**, a **combiner**, and a **reducer**.
    All of those are optional, though ***you must have at least one***.
    """

    def mapper(self, _, line):
        """
        ***Mapper*** takes a key and a value as args
        (in this case, the key is ignored and a single line of text input is the value)
        and yields as many key-value pairs as it likes.
        """

        #Yield is a keyword that is used like return,
        #except the function will return a generator.
        yield "chars", len(line)
        yield "words", len(line.split())
        yield "lines", 1

    def reducer(self, key, values):
        """
        ***Reducer*** takes a key and an iterator of values
        yields as many key-value pairs as it likes
        """
        yield key, sum(values)

# === The main code ===
if __name__ == '__main__':
    print "Ready?"
    MRWordCounter.run()  # run as hadoop your job class
    print "Done!"
