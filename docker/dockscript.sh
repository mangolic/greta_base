#!/bin/bash
echo -e "#######\t@Docking station"

###########################################################
## Config and checks

shared_path="/Users/projects/greta/shared"
path_git="/Users/projects/archivi"
image="pybase:0.1"
pw="-e MYSQL_ROOT_PASSWORD=mysecretpassword"

b2d="/usr/local/bin/boot2docker"
if [ -e $b2d ]; then
	#Commands
	case "$1" in
	  "start")
	    boot2docker up
        exit
	    ;;
	  "stop")
	    boot2docker down
        exit
	    ;;
	esac

	#verify VM status
	case `boot2docker status` in
	    "running")
	    ;;
	    *)
            echo "boot2docker unavailable";
            exit 1;
	esac
else
    #Different path from linux
	shared_path="/opt/project/greta_base/shared"
fi

###########################################################
## Functions

function generatekey {

    dim=32
    # generate two random string of fixed val
    rand1=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $dim | head -n 1)
    rand2=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $dim | head -n 1)
    # create random file name with random file content
    file="/tmp/$rand2"
    echo "$rand1" > $file
    # generate hash from that file ^_^
    key=`sha1sum $file | awk '{print $1}'`
    rm $file

    # return value for bash functions
    echo $key

}

###########################################################
## Make commands - case based on first argument

case "$1" in
  "")
    echo "No command specified"
    ;;

  "check")
    echo ""; echo "*** DOCKER images"; echo "";
    docker images | grep -v ubuntu | grep -v debian;
    echo ""; echo "*** DOCKER processes"; echo "";
    docker ps -a;
    ;;
  "clean")
    com="docker ps -a -q";
    list=`$com | tr "\n" " "`
    if [ ! -z "$list" ]; then
        echo "Stop containers"
        docker stop $($com);
        echo "Delete containers"
        docker rm --volumes=false $($com)
    else
        echo "Nothing to clean"
    fi

    list=`docker images -q --filter "dangling=true" | tr "\n" " " `
    if [ ! -z "$list" ]; then
        echo "Clean untagged images"
        docker rmi $list
    fi
    ;;
  "run")
    if [ -n "$2" ]; then
        image="$2"
        echo "Trying container from image *$image*"
    else
        echo "Creating basic container"
    fi
    docker run -v $shared_path:/usr/local/shared -t -i $image bash -l
    ;;

#####################################

  "key")
    generatekey
  ;;
  "db")
    cdbname="db"
    key=$(generatekey)  #function defined above

# SHOULD BE LAUNCHED ONLY ONCE
# OR FORCE RELOAD
# docker stop $cdbname; docker rm $cdbname;

    echo "Launching ReThinkDb container"
    #not exposing any port. they will only be shared with attached container
    docker run -d --name $cdbname rethinkdb

    # Apply security to rethinkdb Database:
    # http://www.rethinkdb.com/docs/security/

    # Unset any authentication key
    docker exec $cdbname rethinkdb admin unset auth
    # Set the new key
    echo "Security key for this session: *$key*"; sleep 2
    docker exec $cdbname rdbauth $key

    # Set parameters for python application
    opts="--link $cdbname:db -p 5005:5000 -v $shared_path:/usr/local/shared"
    # Recover authentication keypass
    auth="-e KEYDBPASS=$key"

    echo "Linking python container to db"
    #works with GNU screen
    docker run -ti --name py $opts $auth webapp #./myapp
    ;;

# DA MODIFICARE E MIGLIORARE...
  "dbsave")
    echo "Launch data only container"
    list=`docker ps -a | grep data:latest | awk '{print $1}'`
    if [ -z "$list" ]; then

#CAMBIARE
        docker run --name data -v /Users/projects/data:/data:rw -t data
    else
        echo "Data already running"
    fi
    #alternative
    #docker run -i -t -name mysql_data -v /var/lib/mysql busybox /bin/sh

    echo "Launching ReThinkDb container"
    docker run -d --name db --volumes-from data -p 8080:8080 -p 28015:28015 -p 29015:29015 rethinkdb
    echo "Linking python container to db"
    docker run --name py --link db:db -v $shared_path:/usr/local/shared -p 5000:5000 -t -i webapp
    ;;
  "dataonly")
    echo "Launch data only container"
    docker run --name data -v /Users/projects/data:/data:rw -t data
    ;;
  "databackup")
    #requires a running 'data' container, with a /data having some data to backup
    docker run --rm --volumes-from data -v $(pwd):/backup busybox tar cvf /backup/backup.tar /data
    ;;
  "datarestore")
    #launching the data container recovering our backup
    docker run --volumes-from data -v $(pwd):/backup busybox tar xvf /backup/backup.tar
    ;;

  "lampp")

    echo "Launch data?"
    # SHOULD i USE docker create only?
    # docker run -d -v /Users/projects/data/mysql:/var/lib/mysql --name mysql_data ubuntu:12.04 echo Data-only

    echo "Launch mysql"
    # SHOULD i CHECK if already running?
    #docker run -t -i --name mdb -p 3306:3306 --volumes-from mysql_data $pw mysql /bin/bash
    #docker run -d --name mdb -p 3306:3306 --volumes-from mysql_data $pw mysql
    docker run -d --name mdb -p 3306:3306 $pw mysql

    echo "Linking apache+php to db"
    p="/usr/local/shared"
    sleep 2
    docker run -d --name php --link mdb:db -v $path_git:$p -p 80:80 apache
    sleep 2
    #link files in apache
    docker exec php ln -s /usr/local/shared/beta /var/www/html/alpha

    #EXEC BASH?
    ;;

#####################################

  "ip")
    docker inspect $(docker ps -a -q) | grep IPAdd
    ;;
  "exp")
    string="export DOCKER_HOST=tcp://192.168.59.103:2375"
    echo $string
    `$string`
    ;;
  #prefix*)   do_prefix ;;
  *) echo "Unknown command '$1'" ;;
esac

###########################################################
# The end
exit 0;
