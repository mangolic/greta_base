# -*- coding: utf-8 -*-
"""
Impement restful Resources for flask
"""

# Will use the restful plugin instead!
from flask.ext.restful import reqparse, abort, Resource
# Log is a good advice
from bpractices.exceptions import LoggedError
# The global data
from myapi.app import app, g
# Data models from DB ORM
from rdb import data_models

# === HTTP status codes ===
#http://www.w3.org/Protocols/HTTP/HTRESP.html

HTTP_OK_BASIC = 200
HTTP_OK_CREATED = 201
HTTP_OK_ACCEPTED = 202
HTTP_BAD_REQUEST = 400
HTTP_BAD_UNAUTHORIZED = 401
HTTP_BAD_FORBIDDEN = 403
HTTP_BAD_NOTFOUND = 404

# === Implement resources ===

def clean_parameter(param=""):
    """ I get parameters already with '"' quotes from curl... """
    return param.strip('"')


class GenericDBResource(Resource):
    """
    Generic Database API-Resource. Provide simple operations:
    1. List all data inside a noSQL table [GET method]
    2. Add one element inside table [POST method]
    Based on flask-restful plugin
    """

    #define
    def __init__(self, ormModel=data_models.GenericORMModel):
        """ Implement a parser for validating arguments of api """

        # how to get the model as parameter?
        # http://stackoverflow.com/a/26526582

        # Decide the model to use for this DB resource
        g.rdb.define_model(ormModel)
        # Use the model to configure parameters
        self.parser = self.__configure_parsing()

    def __configure_parsing(self):
        """
        Define which data will be retrieved from this resource.
        The arguments are defined automatically from the selected ORM model.
        """

        parser = reqparse.RequestParser()
        # The unique key of the record, could  be forced to an int
        parser.add_argument('id', type=int,
            help='The "id" parameter should be an integer')
        # Warning: if the ID already exists, POST will UPDATE

        # Based on my datamodelled ORM class
        attributes = g.rdb.get_fields()

        # Cycle class attributes
        for key, value in attributes.iteritems():
            print "Attr:", key, value
            # Decide type based on attribute?
            parser.add_argument(key, type=str)

    def get(self, data_key=None):
        """
        Get all data. Note: could be an object serialized:
        restful docs in /quickstart.html#data-formatting
        """
        app.logger.info("API: Received 'search'")

        try:
            if data_key == None:
                # Query ALL
                data = g.rdb.search()
            else:
                data_key = clean_parameter(data_key)
                app.logger.info("API: Received 'search' for " + data_key)
                #Query RDB filtering on a single key
                data = g.rdb.search(by_key=data_key)
        except LoggedError, e:
            abort(HTTP_BAD_NOTFOUND, message=e.__str__())
        return data

    def post(self):
        """ Get data as defined in init parser and push it to rdb """
        data = self.parser.parse_args()
        app.logger.debug("API: POST request open")
        app.logger.debug(data)

        key = data.pop("id")    #gives None if not exists
        key = g.rdb.insert(data, key)
        app.logger.debug("API: Insert of key " + key.__str__())
        return key, HTTP_OK_CREATED

# TO CHECK
    def delete(self, data_key):
        data_key = Data.clean_parameter(data_key)
        app.logger.info("API: Received 'delete' for " + data_key)
        try:
            data = g.rdb.remove(data_key)
        except LoggedError, e:
            abort(HTTP_BAD_NOTFOUND, message=e.__str__())
        return data
# TO FIX - add costant on this number
        return '', 204

"""
    def put(self, todo_id):
        args = parser.parse_args()
        task = {'task': args['task']}
        TODOS[todo_id] = task
        return task, 201
"""

# === Implement resources ===

################################################################
# Get instances of the generic resources
# Based on a specific data_models
# Warning: due to restful plugin system, get and get(value)
# require two different resources...
class DataList(GenericDBResource):
    def __init__(self):
        super(DataList, self).__init__(data_models.DataDump)
class DataSingle(GenericDBResource):
    def __init__(self):
        super(DataSingle, self).__init__(data_models.DataDump)
################################################################

################################################################
################################################################
# FOR FUTURE TESTING on AUTHENTICATION?

# Handle login and logout
class LogUser(Resource):
    """  Init authentication and give token """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user', type=str)
        # IN CHIARO??
        self.parser.add_argument('password', type=str)
            #help='The "id" parameter should be an integer')

    def post(self):
        """ Get data as defined in init parser and push it to rdb """
        data = self.parser.parse_args()
        #print data
        user = data["user"]
        p = data["password"]
        app.logger.info("API: Received login request for user '" + user + "'")

        ###############################
        code = HTTP_BAD_UNAUTHORIZED

# TO FIX
    #check if user is inside database?
        if user in {}:
            if p == "test":
                # Authenticate and log in!
                code = HTTP_OK_ACCEPTED
                msg = "Logged in"
            else:
                msg = "Password is wrong"
        else:
            msg = "Failed to authenticate"

        # Server response
        app.logger.info("API: " + msg + "\t[code " + code.__str__() + "]")
        return msg, code
