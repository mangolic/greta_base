""" Python test with rethinkdb """

import os
import json

import rethinkdb as r
from rethinkdb.errors import RqlRuntimeError, RqlDriverError

#db is the name of the rethinkdb linked container
RDB_HOST = "db"
#linked port or standard rethinkdb port
RDB_PORT = os.environ.get('DB_PORT_28015_TCP_PORT') or 28015
#my data
APP_DB = "webapp"
MAIN_TABLE = "data"

#r.connect(RDB_HOST, RDB_PORT).repl()

def dbSetup():
    """ rethinkdb connection pattern """

    #connect without db to create it if already exists
    connection = r.connect(host=RDB_HOST, port=RDB_PORT)    #should i TRY this?
    try:
        r.db_create(APP_DB).run(connection)
        print "Database connected"
    except RqlRuntimeError:
        print "App database already exists."
    connection.close()

    #now i know the DB exists
    connection = r.connect(host=RDB_HOST, port=RDB_PORT, db=APP_DB)
    try:
        r.table_create(MAIN_TABLE).run(connection)
        print "Table created"
    except RqlRuntimeError:
        print "App table already exists."

    return connection

def insertSomeData(connection):
    """ insert one row inside the table """
    data = {"a":"test", "b":"and", "c":"some other", "d":"data"}
    inserted = r.table(MAIN_TABLE).insert(data).run(connection)
    return inserted['generated_keys'][0]


def getJsonData(connection):
    """ get json data from table """
    selection = list(r.table(MAIN_TABLE).run(connection))
    return json.dumps(selection)

########################################
# MAIN
conn = dbSetup()
r.table(MAIN_TABLE).delete().run(conn)
print insertSomeData(conn) + " created inside " + MAIN_TABLE + "\n"
print getJsonData(conn)

#END (+EOL)
